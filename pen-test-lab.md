
## Step-by-Step Guide for Building a Pen-Test Lab

- Verify the checksum of downloaded Virtual Machine for intergrity check

- Ping virtual machine to test for internet connectivity. [3.How-To](https://github.com/komal101/CheatSheets/edit/master/CheatSheets/Linux/useful-commands.md) | [1.Troubleshoot](https://github.com/komal101/CheatSheets/blob/master/TroubleShoot.md)

- Use the commands: `apt-get clean && apt-get update && apt-get upgrade -y && apt-get dist-upgrade -y` 
  - `apt-get clean` clears out the cached files.
  - `apt-get update` is run before installing any packages. This ensures the list of available packages and their versions are up-to-date. [2.Troubleshoot](https://github.com/komal101/CheatSheets/blob/master/TroubleShoot.md)
  - `apt-get upgrade` to install the current versions of the packages you have under the list of available packages. [2.Troubleshoot](https://github.com/komal101/CheatSheets/blob/master/TroubleShoot.md)
  - `apt-get autoremove` to automatically remove the unwanted packages

- Create user account, enlist in sudo group & change its default shell to bash shell. [1.How-To](https://github.com/komal101/CheatSheets/blob/master/CheatSheets/Linux/useful-commands.md)

- Configure Date & Time Settings

- Install vmware tools

- Enable PostgreSQL and Metasploit system services [2.How-To](https://github.com/komal101/CheatSheets/blob/master/CheatSheets/Linux/useful-commands.md)

- Test if Copy & Paste works between Host and VM

- Install openvpn (if required)

- Finally, take a Snapshot of the machine state
