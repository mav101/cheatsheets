# Pentesting CheatSheets

### [Building Pen-Test Lab for Security Research - Step-by-Step Guide](https://github.com/komal101/CheatSheets/blob/master/pen-test-lab.md)

### [Guide to Cyber Security Research for Finding Vulnerabilities](https://github.com/komal101/CheatSheets/blob/master/CS-Research.md)

* [Linux System Enumeration](https://github.com/komal101/CheatSheets/blob/master/CheatSheets/Linux/System-enum.md)
* [Windows System Enumeration](https://github.com/komal101/CheatSheets/blob/master/CheatSheets/Windows/System-enum.md)
* [Web Application](https://github.com/komal101/CheatSheets/blob/master/CheatSheets/WebApp-Pentest/Linux-Commands.md)
* [Infrastructure](https://github.com/komal101/CheatSheets/blob/master/CheatSheets/Infrastructure/Ports.md)
* [Forensics](https://github.com/komal101/CheatSheets/blob/master/CheatSheets/Forensics/Forensics.md)
* [Test Scripts](https://github.com/komal101/CheatSheets/blob/master/CheatSheets/TestScripts/TestScripts.md)

### OWASP TOP 10 Vulnerabilities

1. [Injections](https://github.com/komal101/CheatSheets/blob/master/CheatSheets/WebApp-Pentest/Injections.md)
   * **SQLi**
   * **Code Injection**
   * **OS Command Execution**
   * **LDAP**
   * **XML**
   * **XPath**
   * **Content Injection**
        * [HTB-Enterprise](https://github.com/komal101/CTF-Writeups/blob/master/HTB/Enterprise-Enumeration-Stage.pdf)
                              
2. [Broken Authentication & Session Management]()
    * <b>Test<b>
3. [XSS](https://github.com/komal101/CheatSheets/tree/master/CheatSheets/WebApp-Pentest/XSS.md)
4. #### Broken Access Control ####
      * <b>Directory Traversal</b>
      * <b>Unrestricted File Upload</b>
           * ##### [HTB-Nibbles](https://github.com/komal101/CTF-Writeups/blob/master/HTB/Nibbles-Exploited.pdf) #####
           
5. [Security Misconfiguration]()
6. [Sensitive Data Exposure]()
7. [Insufficient Attacks Protection]()
8. [CSRF]()
9. [Components with known Vulnerabilities]()
      i. ShellShock
      ii. HeartBleed
      
10. [Unprotected APIs]()

### Other WebApp Attack Vectors

* [LFI/RFI]() (Local/Remote File Inclusion)
* [Unrestricted file Upload]() 
* [RCE (Remote Command Execution)]()
* [Null session (Windows)]()
* [Buffer Overflow]()

### Partition & Filesystem

* [Linux](https://github.com/komal101/CheatSheets/blob/master/CheatSheets/Linux/System-enum.md)
* [Windows]


### PenTest Practice Platform

* [Websites](https://github.com/komal101/CheatSheets/blob/master/Websites.md)

### Report Writing

* [OffSec Style Report](https://github.com/komal101/CheatSheets/blob/master/sample-penetration-testing-report.pdf)

### Certs

* [CEH](https://github.com/komal101/CheatSheets/blob/master/CEH%20Exercises.pdf)

### MarkDown(.md) File Style Guide

* Title ```#``` 
* Title Section ```##```
* Subheadings ```**SH**``` 
* Code blocks ``` three ticks before and after a text
* Links ``` [clickable text](url-link-here) ``` 
