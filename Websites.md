
## Educational

1. ```https://www.hacksplaining.com/``` Learn about different types of vulnerabilities


## News
1. ```www.zerosecurity.org``` 


## Registered Domains

1. ```www.dailychanges.com/new-domains```
2. ```http://tastereports.com/index.html```


## Other Useful Sites

1. ```www.virustotal.com```
