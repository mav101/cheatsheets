# FORENSICS #

 
 ### ANALYSING NETWORK TRAFFIC ###
   * ```tcpdump -r file.pcap```
   * ```tcpdump -i <interface>```
 
 ### DATA RECOVERY ###
   * ```foremost```

 ### FILE EXTRACTION ###
 
 * ```gunzip``` - .gz
 
 ### IMAGES ###
   * ```exiftool -comment='hi' imagefile.jpg```
   
 ### PASSWORD CRACKING ###
 
   #### Protected Zip Folder Password Recovery ####
   * ``` fcrackzip -u -c a -D -p /usr/share/wordlists/rockyou.txt '/full/path/to/password/protected/zipfolder' ``` </br>
   -u attempt to unzip with password guessing </br>
   -c characters set (alpha or alphanummeric) </br>
   -p initial password string or wordlist file </br>
   -D Launches Dictionary attack </br>

   ### WORDPRESS Password Recovery ####
   
   * ``` sudo wpscan -u 192.x.x.x --threads 10 --wordlist /usr/share/wordlists/rockyou.txt --username <user> ```
   
   #### PDF Password Recovery ####
   
   * ``` pdfcrack test.txt -w /usr/share/wordlists/rockyou.txt ```
   
   #### Hashed Password Recovery ####

   * ```sudo hashcat -m 1400 -a 3 /path/to/hashfile.txt /usr/share/wordlists/rockyou.txt --force```
   * ```sudo john --wordlist=/usr/share/wordlists/rockyou.txt pass.txt```</br> 
        ```john --show hashes.txt```
   * ```ssh2john``` Converts the ssh encrypted key to john format for password cracking
