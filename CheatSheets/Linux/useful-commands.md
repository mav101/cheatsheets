## How-To

### 1. Account Creation

* sudo useradd -m <username>
* sudo passwd Komz <hit enter and then type Password>
 
(Note: -m means create home directory which is /home/username) 

* Now set password for this user 
passwd <username> 
 
* Add user to sudo group (to allow user to install software, allow printing, use privileged mode etc.) 
usermod -a -G sudo <username>
 
(Note: -a means append or add and �G mean to specified group/groups) 
* Change default shell of previously created user to bash 
chsh -s /bin/bash user1 
 
(Note: chsh mean change login shell, -s is the name of the specified shell you want for the user, in this case /bin/bash) 

Logout and login back as our new Standard Non-root user.

### 2. Fix Metasploit slow search

   ##### To Enable PostgreSQL and Metasploit system services

`service postgresql start` <br/>
`service metasploit start`

##### To enable the service to start during re-boot

`update-rc.d postgresql enable` <br/>
`update-rc.d metasploit enable`

##### To Rebuild Metasploit cache from msfconsole prompt

`db_rebuild_cache`

### 3. Ping from Virtual Machine

* ping <localhost>
* ping <DNS(Domain Name System), DGW (Default Gateway)>
* ping <IP & Something by Name>

### 4. Log Data

* `tcpdump -i <Interface i.e. eth0> -w <DataOutput-filename > -s <Packet Size in Bytes. Max 65535 to record the whole packet> host <hostname> and port <portNumber>`

### 5. Import test scripts from my bitbucket
