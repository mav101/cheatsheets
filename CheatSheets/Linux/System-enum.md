# SYSTEM ENUMERATION

Some useful System Info & Process Management commands:

**SYS-ENUM**
  
 * ```ifconfig ```
 * ```who```
 * ```lsb_release -a``` OS Release
 * ```whoami``` who you are/logged in as
 * ```uname -a``` Kernel config info
 * ```which <app>``` ```locate <app>``` possible location of app
 * ```whereis <app>``` - which app will be run by default
 * ```cat /proc/version```
 * ```cat /etc/*release``` ```lsb_release -a``` Distribution version
 * ```sudo su``` ```sudo su -``` ```sudo -l``` ```su -```
 * ```sudo -u username``` switch to another username
 * ```wget https://highon.coffee/downloads/linux-local-enum.sh``` A complete sys-enum scanning script
 
 
**SHELL ESCAPE SEQUENCES** 
 
 * vi-->  ```:!bash```
 * vi--> ```:set shell=/bin/bash:shell```  
            <br/> 
                  ```:shell```
       
 * awk--> ```awk 'BEGIN {system("/bin/bash")}'```

 * find--> ```find / -exec /usr/bin/awk 'BEGIN {system("/bin/bash")}' \;```

 * perl--> ```perl -e 'exec "/bin/bash";'``` 
 
**PROCESS MANAGEMENT**
    
 * ```ps```
 * ```netstat -antp```

**RESTRICTED BASH**

 * ```bash```
 * ```compgen -c``` lists down all available command for this shell
  
**TTY SHELL SPAWNING** - To interact further with the system

 * ```python -c 'import pty; pty.spawn("/bin/bash")'```
 * ```/bin/sh -i```
 * ```echo os.system('/bin/bash')```

**FILE TRANSFERS** 
  
   Before making a file transfer via HTTP, make sure to start the HTTP Service on the local machine using either```python -m simpleHTTPServer``` or ```sudo service apache2 start/restart```
    
  * ```wget http://<local-ip>/file.txt -O /var/www/html/file.txt```
  * ```wget http://<remote-ip>/remote-file.txt```

**IMPORTANT FILE PATH**

  * ```/etc/issue```
  * ```/var/mail``` 
  * ```/proc/version```
  * ```/etc/profile```
  * ```/etc/passwd```
  * ```/etc/shadow```
  * ```/root/.bash_history```
  * ```/var/log/dmessage```
  * ```/var/mail/root```
  * ```/var/spool/cron/crontabs/root```
  
**PARTITION & FILESYSTEM**

  A partition is a logical part of a hard disk or other secondary storage. This allows an operating system to manage information in each region separately.
  
  A filesystem is a method of storing / finding files inside a disk partition.
  
  * ```fdisk -l``` lists the partition table of the specified device.
  
```/dev/sda``` is the first hard drive (the primary master), ```/dev/sdb``` is the second etc. 
```/dev/sda1``` is the first partition of the first drive, ```/dev/sda2``` is the second partition of the first drive etc.

  To find out the name of your (attached) USB drive, run ```sudo fdisk -l```
  
  #### Lost File Recovery
  
  e.x: recover data from /dev/sdb on /media/usbstick
          
   * ```strings /dev/sbd```
   * ```xxd /dev/sdb```
   * ```grep -a '[a-z0-9]\{32\}' /dev/sdb```


    
